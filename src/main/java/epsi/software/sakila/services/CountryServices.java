package epsi.software.sakila.services;

import epsi.software.sakila.entities.Country;

import java.util.List;

/**
 * La classe Interface
 */
public interface CountryServices {
    Country create(Country country);
    Country read(Long id);
    List<Country> readAll();
    Country update(Country country);
    boolean delete(Long id);
}
