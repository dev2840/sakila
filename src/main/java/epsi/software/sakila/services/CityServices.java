package epsi.software.sakila.services;

import epsi.software.sakila.entities.City;

import java.util.List;

/**
 * La classe Interface
 */
public interface CityServices {
    City create(City city);
    City read(Long id);
    List<City> readAll();
    City update(City city);
    boolean delete(Long id);
}
