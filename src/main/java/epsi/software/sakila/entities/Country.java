package epsi.software.sakila.entities;

import jakarta.persistence.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
    @Table(name="country")
public class Country {

    @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name="country_id")
    private Long id;

    @Column(name="last_update")
    private LocalDateTime lastUpdate;

    private String country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }





}
